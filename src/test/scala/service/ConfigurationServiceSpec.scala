package re.pescato.tps
package service

import integration._

import cats.effect._
import cats.effect.testkit.TestControl
import cats.implicits.catsSyntaxOptionId
import weaver._

import scala.concurrent.duration._

object ConfigurationServiceSpec extends SimpleIOSuite {

  val configurationService = ConfigurationService.apply()

  test("Configuration Service must update configuration accordingly") {
    TestControl.execute(configurationService).flatMap { control =>
      for {
        _ <- control.tickOne
        config <- configurationService.flatMap(_.getConfiguration())
        checkOne <- IO(assert(config.tries == 10 && config.secondsDelay == 10))
        config <- configurationService.flatMap(_.updateConfiguration(AppConfiguration(20, 999)))
        checkTwo <- IO(assert(config.tries == 20 && config.secondsDelay == 999))
      } yield checkOne && checkTwo
    }

  }

}
