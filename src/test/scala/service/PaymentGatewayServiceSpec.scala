package re.pescato.tps
package service

import integration._

import cats.effect._
import cats.effect.testkit.TestControl
import cats.implicits.catsSyntaxOptionId
import weaver._

import scala.concurrent.duration._

object PaymentGatewayServiceSpec extends SimpleIOSuite {

  val pgs: PaymentGateway = new MockPaymentGatewayService()

  test("Payment Gateway Service returns Successful response") {
    val request = TransferRequest(10d, 1234l, 1)
    val program = pgs.enterTransfer(request)

    TestControl.execute(program).flatMap { control =>
      for {
        _ <- control.tickOne
        response <- program
        status <- pgs.checkTransferStatus(response.id)
        checkOne <- IO(assert(status == Pending.some))
      } yield checkOne
    }

  }

  test("Payment Gateway Service returns Failed response because amount is less than zero") {
    val request = TransferRequest(-10d, 1234l, 1)
    val program = pgs.enterTransfer(request)

    TestControl.execute(program).flatMap { control =>
      for {
        _ <- control.tickOne
        response <- program
        status <- pgs.checkTransferStatus(response.id)
        checkOne <- IO(assert(status == Failure.some))
      } yield checkOne
    }
  }

  test("Payment Gateway Service updates the state to Success after a while") {
    val request = TransferRequest(10d, 1234l, 1)
    val service = new MockPaymentGatewayService()

    TestControl.execute(service.startTransferUpdater()).flatMap { control =>
      for {
        // starting the program
        _ <- control.tick

        // simulating three transfer requests
        response <- service.enterTransfer(request)
        response2 <- service.enterTransfer(request)
        responseFailure <- service.enterTransfer(TransferRequest(-11d, 1234l, 1)) //failing request
        response3 <- service.enterTransfer(request)

        status <- service.checkTransferStatus(response.id)
        status2 <- service.checkTransferStatus(response2.id)
        failedStatus <- service.checkTransferStatus(responseFailure.id)
        status3 <- service.checkTransferStatus(response3.id)
        checkOne <- IO(assert(status == Pending.some))
        checkTwo <- IO(assert(status2 == Pending.some))
        checkThree <- IO(assert(status3 == Pending.some))
        checkFailed <- IO(assert(failedStatus == Failure.some)) //failed request

        // simulating the stream checking
        _ <- control.advanceAndTick(6.seconds)

        // verifying that after the stream check all the data have been properly updated to Success
        updatedStatus <- service.checkTransferStatus(response.id)
        updatedStatus2 <- service.checkTransferStatus(response2.id)
        updatedStatus3 <- service.checkTransferStatus(response3.id)
        updatedFailedStatus <- service.checkTransferStatus(responseFailure.id)
        checkFour <- IO(assert(updatedStatus == Success.some))
        checkFive <- IO(assert(updatedStatus2 == Success.some))
        checkSix <- IO(assert(updatedStatus3 == Success.some))
        checkFailedAfterUpdate <- IO(assert(updatedFailedStatus == Failure.some)) // failed request did not change its state
      } yield checkOne && checkTwo && checkThree && checkFour && checkFive && checkSix && checkFailed && checkFailedAfterUpdate
    }
  }

}
