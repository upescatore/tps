package re.pescato.tps
package service

import persistence.BankTransferService

import cats.effect.{IO, Resource}
import cats.implicits.catsSyntaxOptionId
import doobie.hikari.HikariTransactor
import weaver.{GlobalRead, IOSuite, LowPriorityImplicits}

import java.util.UUID

class BankTransferServiceSpec(global: GlobalRead) extends IOSuite with LowPriorityImplicits {

  /* This is for sharing the resource among test cases */

  override type Res = HikariTransactor[IO]
  override def sharedResource: Resource[IO, Res] =
    global.getOrFailR[HikariTransactor[IO]](None)(classBasedInstance)

  test("Add pending bank transfer and remove it") { xa =>
    val bankTransferService = BankTransferService(xa)
    val transferUUID = UUID.randomUUID()

    for {
      _ <- bankTransferService.addPendingBankTransfer(transferUUID, 100.0, 1, 1, 1)
      retrievedTransfer <- bankTransferService.getByUUID(transferUUID)
      checkOne <- IO(expect(retrievedTransfer.map(_.transferState) == "Pending".some))
      _ <- bankTransferService.deleteByUUID(transferUUID)
      deleted <- bankTransferService.getByUUID(transferUUID)
      checkTwo <- IO(expect(deleted.isEmpty))
    } yield checkOne && checkTwo

  }

}
