package re.pescato.tps
package service

import integration.{MockPaymentGatewayService, Pending}
import persistence.{AccountService, BankTransferService}

import cats.effect.testkit.TestControl
import cats.effect.{IO, Resource}
import cats.implicits.{catsSyntaxOptionId, catsSyntaxTuple2Parallel}
import doobie.hikari.HikariTransactor
import weaver.{GlobalRead, IOSuite, LowPriorityImplicits}

import scala.concurrent.duration.DurationInt

class TransferServiceSpec(global: GlobalRead) extends IOSuite with LowPriorityImplicits {

  /* This is for sharing the resource among test cases */

  override type Res = HikariTransactor[IO]
  override def sharedResource: Resource[IO, Res] =
    global.getOrFailR[HikariTransactor[IO]](None)(classBasedInstance)

  test("Check Bank Transfer flow") { xa =>
    val bankTransferService = BankTransferService(xa)
    val gatewayMock = new MockPaymentGatewayService()
    val account = new AccountService(xa)
    val configuration = ConfigurationService.unsafe

    val transferService = TransferService.impl(gatewayMock, bankTransferService, account, configuration)

    val accountNumber = 10
    val deductedAmount = 20
    val transfer = EnteredTransfer(deductedAmount, accountNumber, 1, 1)

    val program = (gatewayMock.startTransferUpdater(), transferService.checkTransferStatus()).parTupled

    TestControl
      .execute(program)
      .flatMap { control =>
        for {
          _ <- control.tickOne
          accountAmountBalanceBeforeTransfer <- account.getByAccountNumber(accountNumber).map(_.map(_.balance))
          response <- transferService.processTransfer(transfer)
          checkOne <- IO(expect(response.status == Pending))
          _ <- control.tickFor(120.seconds)
          dbResponse <- transferService.getTransfer(TransferReference(response.id))
          checkTwo <- IO(expect(dbResponse.map(_.transferState) == "Success".some))
          accountAmountBalanceAfterTransfer <- account.getByAccountNumber(accountNumber).map(_.map(_.balance))
          // check that after the flow the amount of the balance is properly deducted
          checkThree <- IO(
            expect(accountAmountBalanceBeforeTransfer == accountAmountBalanceAfterTransfer.map(_.+(deductedAmount))))
        } yield checkOne && checkTwo && checkThree
      }
  }

}
