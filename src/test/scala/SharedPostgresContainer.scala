package re.pescato.tps

import cats.effect.{IO, Resource}
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import org.testcontainers.containers.PostgreSQLContainer
import weaver.{GlobalResource, GlobalWrite, LowPriorityImplicits}

object SharedPostgresContainer extends GlobalResource with LowPriorityImplicits {

  def postgreSQLContainer() =
    new PostgreSQLContainer("postgres:latest")
      .withInitScript("sql/init.sql")
      .asInstanceOf[PostgreSQLContainer[_]]
//      .withDatabaseName("tpsdb")
//      .asInstanceOf[PostgreSQLContainer[_]]

  private val container = Resource.make(IO(postgreSQLContainer()).flatMap { c =>
    c.start()
    IO.println(s"Started postgresql container ${c.getJdbcUrl}")
    IO.pure(c)
  }) { c =>
    IO.println(s"Closing postgresql container ${c.getJdbcUrl}")
    IO(c.stop())
  }

  override def sharedResources(global: GlobalWrite): Resource[IO, Unit] =
    for {
      c <- container
      ce <- ExecutionContexts.fixedThreadPool[IO](5)
      xa <- HikariTransactor.newHikariTransactor[IO](
        "org.postgresql.Driver",
        c.getJdbcUrl,
        c.getUsername,
        c.getPassword,
        ce
      )
      // this service returns Transactor[IO]
      _ <- global.putR(xa)(classBasedInstance)
    } yield ()

}
