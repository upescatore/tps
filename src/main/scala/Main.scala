package re.pescato.tps

import http.TPSServer.serverLogic

import cats.effect.{IO, IOApp, Resource}
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts

object Main extends IOApp.Simple {
  override def run: IO[Unit] = {

    //this is left vanilla for the sake of the exercise.
    // Please use proper Typesafe config or Pureconfig in a real project
    val postgres: Resource[IO, HikariTransactor[IO]] = for {
      ce <- ExecutionContexts.fixedThreadPool[IO](32)
      xa <- HikariTransactor.newHikariTransactor[IO](
        "org.postgresql.Driver",
        "jdbc:postgresql://localhost:5444/tpsdb",
        "docker",
        "docker",
        ce
      )
    } yield xa

    postgres.use { transactor =>
      serverLogic(transactor)
    }

  }
}
