package re.pescato.tps
package http

import integration.MockPaymentGatewayService
import service.{ConfigurationService, TransferService}

import cats.effect.{ExitCode, IO}
import cats.implicits.{catsSyntaxTuple3Parallel, toSemigroupKOps}
import com.comcast.ip4s.IpLiteralSyntax
import doobie.hikari.HikariTransactor
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.middleware.Logger
import org.typelevel.log4cats.SelfAwareStructuredLogger
import re.pescato.tps.persistence.{AccountService, BankTransferService}

/**
 * Singleton containing all the wiring to configure and start the server at the end of the world.
 */
object TPSServer {

  val logger: SelfAwareStructuredLogger[IO] = logging.getLogger

  def serverLogic(transactor: HikariTransactor[IO]) = {

    val gatewayService = new MockPaymentGatewayService()

    for {
      _ <- logger.info("Starting Server")
      configurationService <- ConfigurationService()
      bankService = BankTransferService(transactor)
      accountService = AccountService(transactor)
      transferService = TransferService.impl(gatewayService, bankService, accountService, configurationService)
      httpApp = (
        ServerRoutes.transferRoutes(transferService) <+>
          ServerRoutes.configTransferRoutes(configurationService)
      ).orNotFound

      finalHttpApp = Logger.httpApp(true, true)(httpApp)

      _ <- (
        gatewayService.startTransferUpdater(),
        transferService.checkTransferStatus(),
        EmberServerBuilder
          .default[IO]
          .withHost(ipv4"0.0.0.0")
          .withPort(port"8080")
          .withHttpApp(finalHttpApp)
          .build
          .use(_ => IO.never)
          .as(ExitCode.Success)).parTupled

    } yield ()
  }

}
