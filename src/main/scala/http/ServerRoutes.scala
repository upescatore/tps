package re.pescato.tps
package http

import service._

import cats.effect._
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.dsl.io._

object ServerRoutes {

  /**
   * Endpoints for the transfers
   * @param transferService
   * @return
   */
  def transferRoutes(transferService: TransferService): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case req @ POST -> Root / "enter-transfer" =>
        req
          .decode[EnteredTransfer] { transfer =>
            // Process the transfer
            transferService
              .processTransfer(transfer)
              .flatMap { gatewayResponse =>
                if (gatewayResponse.isPending) {
                  Ok(gatewayResponse)
                } else InternalServerError(s"Transfer rejected > ${gatewayResponse.status}")
              }
              .handleErrorWith { case x: Throwable =>
                InternalServerError(s"Error while processing the request > ${x.getLocalizedMessage}")
              }
          }
          .handleErrorWith { case x: Throwable =>
            InternalServerError(s"Error while validating the request > ${x.getLocalizedMessage}")
          }

      //get the state of the transfer by transfer reference
      case req @ GET -> Root / "transfer" =>
        req
          .decode[TransferReference] { reference =>
            transferService
              .getTransfer(reference)
              .flatMap {
                case Some(transfer) => Ok(transfer)
                case None => NotFound(s"Transfer ${reference.reference} not found")
              }
              .handleErrorWith { case x: Throwable =>
                InternalServerError(s"Error while processing the request > ${x.getLocalizedMessage}")
              }
          }
          .handleErrorWith { case x: Throwable =>
            InternalServerError(s"Error while validating the request > ${x.getLocalizedMessage}")
          }
    }

  /**
   * Endpoint to get and update configuration settings
    */
  def configTransferRoutes(configurationService: ConfigurationService): HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / "config-transfer" =>
      configurationService
        .getConfiguration()
        .flatMap(updatedConfig =>
          Ok(s"Current configuration: Tries=${updatedConfig.tries}, Delay=${updatedConfig.secondsDelay}"))
        .handleErrorWith { case x: Throwable =>
          InternalServerError(s"Error while processing the request > ${x.getLocalizedMessage}")
        }

    case req @ POST -> Root / "config-transfer" =>
      req
        .decode[AppConfiguration] { newConfig =>
          configurationService
            .updateConfiguration(newConfig)
            .flatMap(updatedConfig =>
              Ok(s"Updated configuration: Tries=${updatedConfig.tries}, Delay=${updatedConfig.secondsDelay}"))
            .handleErrorWith { case x: Throwable =>
              InternalServerError(s"Error while updating the request > ${x.getLocalizedMessage}")
            }
        }
        .handleErrorWith { case x: Throwable =>
          InternalServerError(s"Error while validating the request > ${x.getLocalizedMessage}")
        }
  }
}
