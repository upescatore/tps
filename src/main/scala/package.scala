package re.pescato

import cats.effect.IO
import org.typelevel.log4cats.LoggerFactory
import org.typelevel.log4cats.slf4j.Slf4jFactory

package object tps {

  // implicit logger factory to use through the application
  implicit val logging: LoggerFactory[IO] = Slf4jFactory[IO]

}
