package re.pescato.tps
package persistence

import cats.effect.IO
import doobie.Transactor
import fs2.Stream

import java.util.UUID
import doobie._
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import persistence.domain.BankTransfer

class BankTransferService(transactor: Transactor[IO]) {
  def addPendingBankTransfer(
    transferUUID: UUID,
    transferredAmount: Double,
    senderAccountNumber: Long,
    recipientBankCode: Int,
    recipientAccountNumber: Int
  ): IO[Unit] = addBankTransfer(
    BankTransfer(
      transferUUID,
      "Pending",
      transferredAmount,
      senderAccountNumber,
      recipientBankCode,
      recipientAccountNumber))
  private def addBankTransfer(transfer: BankTransfer): IO[Unit] =
    sql"""
      |INSERT INTO bank_transfers (transfer_uuid, transfer_state, transferred_amount, sender_account_number, recipient_bank_code, recipient_account_number)
      |VALUES (${transfer.transferUUID}, ${transfer.transferState}, ${transfer.transferredAmount}, ${transfer.senderAccountNumber}, ${transfer.recipientBankCode}, ${transfer.recipientAccountNumber})
      |""".stripMargin.update.run.transact(transactor).void

  def updateBankTransferState(transferUUID: UUID, newState: String): IO[Unit] =
    sql"""
      |UPDATE bank_transfers
      |SET transfer_state = $newState
      |WHERE transfer_uuid = $transferUUID
      |""".stripMargin.update.run.transact(transactor).void

  def getByUUID(transferUUID: java.util.UUID): IO[Option[BankTransfer]] =
    sql"SELECT transfer_uuid, transfer_state, transferred_amount, sender_account_number, recipient_bank_code, recipient_account_number FROM bank_transfers WHERE transfer_uuid = $transferUUID"
      .query[BankTransfer]
      .option
      .transact(transactor)

  def updateTransferState(transferUUID: java.util.UUID, newState: String): IO[Int] =
    sql"UPDATE bank_transfers SET transfer_state = $newState WHERE transfer_uuid = $transferUUID".update.run
      .transact(transactor)

  def deleteByUUID(transferUUID: java.util.UUID): IO[Int] =
    sql"DELETE FROM bank_transfers WHERE transfer_uuid = $transferUUID".update.run
      .transact(transactor)

  def getPendingBankTransfers: Stream[IO, BankTransfer] =
    sql"""
      |SELECT transfer_uuid, transfer_state, transferred_amount, sender_account_number, recipient_bank_code, recipient_account_number
      |FROM bank_transfers
      |WHERE transfer_state = 'Pending'
      |""".stripMargin.query[BankTransfer].stream.transact(transactor)
}

object BankTransferService {
  def apply(transactor: Transactor[IO]): BankTransferService = new BankTransferService(transactor)
}
