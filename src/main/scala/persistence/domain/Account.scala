package re.pescato.tps
package persistence.domain

case class Account(accountNumber: Long, balance: BigDecimal)
