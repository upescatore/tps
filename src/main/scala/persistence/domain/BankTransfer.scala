package re.pescato.tps
package persistence.domain

// Define the model for account and transfer
// Define case classes to represent your data models

case class BankTransfer(
  transferUUID: java.util.UUID,
  transferState: String,
  transferredAmount: Double, // big decimal in real world
  senderAccountNumber: Long,
  recipientBankCode: Int,
  recipientAccountNumber: Int)
