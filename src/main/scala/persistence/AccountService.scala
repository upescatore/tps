package re.pescato.tps
package persistence
import cats.effect._
import doobie._
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import re.pescato.tps.persistence.domain.Account

/**
 * Doobie class for CRUD operations on account tables
 * @param transactor
 */
class AccountService(transactor: Transactor[IO]) {

  def insert(account: Account): IO[Int] =
    sql"INSERT INTO accounts (account_number, balance) VALUES (${account.accountNumber}, ${account.balance})".update.run
      .transact(transactor)

  def getByAccountNumber(accountNumber: Long): IO[Option[Account]] =
    sql"SELECT account_number, balance FROM accounts WHERE account_number = $accountNumber"
      .query[Account]
      .option
      .transact(transactor)

  def decreaseBalance(accountNumber: Long, newBalance: BigDecimal): IO[Int] =
    sql"UPDATE accounts SET balance = balance - $newBalance WHERE account_number = $accountNumber".update.run
      .transact(transactor)

  def updateBalance(accountNumber: Long, newBalance: BigDecimal): IO[Int] =
    sql"UPDATE accounts SET balance = $newBalance WHERE account_number = $accountNumber".update.run
      .transact(transactor)

  def deleteByAccountNumber(accountNumber: Long): IO[Int] =
    sql"DELETE FROM accounts WHERE account_number = $accountNumber".update.run
      .transact(transactor)
}

object AccountService {
  def apply(transactor: Transactor[IO]): AccountService = new AccountService(transactor)
}
