package re.pescato.tps
package service

import cats.effect.{IO, Ref}
import re.pescato.tps.integration.TransferResponse

import java.util.UUID
import scala.concurrent.duration.DurationInt

/**
 * In memory configuration of the application, with default values
 * @param tries
 * @param secondsDelay
 */
case class AppConfiguration(tries: Int = 10, secondsDelay: Int = 10)

/**
 * Naive service to update the configuration locally, in concurrent fashion leveraging Cats-effect features
 */
class ConfigurationService private () {

  // using unsafe since it is a plain case class
  private val configuration = Ref.unsafe[IO, AppConfiguration] {
    AppConfiguration()
  }

  //  Ref[IO].unsafe(AppConfiguration())

  def getConfiguration(): IO[AppConfiguration] =
    configuration.get

  def updateConfiguration(newConfig: AppConfiguration): IO[AppConfiguration] =
    configuration.updateAndGet(_ => newConfig)

  def tries = getConfiguration().map(_.tries)
  def secondsDelay = getConfiguration().map(_.secondsDelay.seconds)
}

object ConfigurationService {
  def apply(): IO[ConfigurationService] = IO(new ConfigurationService)

  // not exposed outside, used only for testing purposes
  private[service] def unsafe = new ConfigurationService
}
