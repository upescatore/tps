package re.pescato.tps
package service

import integration.{Failure, PaymentGateway, Pending, Success, TransferRequest, TransferResponse}

import cats.effect._
import fs2.Stream
import cats.implicits.{catsSyntaxTuple2Parallel, toTraverseOps}
import org.typelevel.log4cats.SelfAwareStructuredLogger
import re.pescato.tps.persistence.BankTransferService
import re.pescato.tps.persistence.AccountService
import re.pescato.tps.persistence.domain.BankTransfer

import java.time.Instant
import java.util.UUID
import scala.concurrent.duration.DurationInt

case class EnteredTransfer(
  transferredAmount: Double,
  senderAccountNumber: Long,
  recipientBankCode: Int,
  recipientAccountNumber: Int)

case class TransferReference(reference: UUID)

trait TransferService {

  /**
   * Business logic to process a transfer by sending it to a third party service
   * @param transfer
   * @return the response of the transfer
   */
  def processTransfer(transfer: EnteredTransfer): IO[TransferResponse]

  /**
   * Business logic to get the current state of a bank transfer given a Transfer Reference
   * @param transferReference
   * @return the state of the corresponding bank transfer, if any
   */
  def getTransfer(transferReference: TransferReference): IO[Option[BankTransfer]]

  /**
   * Infinite stream to check the current status of all the pending transactions
   */
  def checkTransferStatus(): IO[Unit]
}

// Define the service to handle transfers
object TransferService {
  def apply(implicit ev: TransferService): TransferService = ev

  val logger: SelfAwareStructuredLogger[IO] = logging.getLogger

  def impl(
    gatewayService: PaymentGateway,
    bankTransferService: BankTransferService,
    accountService: AccountService,
    configurationService: ConfigurationService
  ) = new TransferService {

    override def processTransfer(transfer: EnteredTransfer): IO[TransferResponse] = (for {
      transferRequest <- IO(
        TransferRequest(transfer.transferredAmount, transfer.senderAccountNumber, transfer.recipientAccountNumber))
      response <- gatewayService.enterTransfer(transferRequest)
      _ <-
        if (response.isPending)
          bankTransferService.addPendingBankTransfer(
            response.id,
            response.amount,
            response.senderAccount,
            transfer.recipientBankCode,
            transfer.recipientAccountNumber)
        else IO.pure()
    } yield response).onError { case _: Throwable =>
      IO.raiseError(new RuntimeException("Error while persisting the response"))
    }

    private def updateEntry(transferResponseOpt: Option[TransferResponse]) =
      transferResponseOpt.map { case response =>
        response.status match {
          case Pending => IO.pure()
          case Success =>
            for {
              _ <- logger.info(s"******** Update account ${response.id} ********")
              _ <- (
                bankTransferService.updateBankTransferState(response.id, response.status.toString),
                accountService.decreaseBalance(response.senderAccount, response.amount)).parTupled
            } yield ()
          case Failure => bankTransferService.updateBankTransferState(response.id, response.status.toString)
        }
      }.sequence

    override def checkTransferStatus(): IO[Unit] =
      Stream
        .repeatEval(configurationService.getConfiguration())
        .flatMap { configuration =>
          Stream.eval(logger.info(
            s"checking status of transfers for ${configuration.tries} times, at every ${configuration.secondsDelay} seconds")) ++
            Stream
              .fixedRate[IO](configuration.secondsDelay.seconds)
              .take(configuration.tries)
              .evalTap(_ => logger.info(s"getting pending transfers ${Instant.now()}"))
              .flatMap(_ => bankTransferService.getPendingBankTransfers)
              .evalMap(transferResponse => gatewayService.getTransferResponse(transferResponse.transferUUID))
              .evalMap(optResponse => updateEntry(optResponse))
        }
        .compile
        .drain

    override def getTransfer(transferReference: TransferReference): IO[Option[BankTransfer]] =
      bankTransferService.getByUUID(transferReference.reference)
  }
}
