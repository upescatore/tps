package re.pescato.tps
package integration

import cats.effect.std.{Random, UUIDGen}
import cats.effect.{IO, Ref}
import cats.implicits._
import fs2.Stream
import org.typelevel.log4cats.SelfAwareStructuredLogger

import java.util.UUID
import scala.concurrent.duration.{Duration, FiniteDuration, MILLISECONDS}

/**
 * Interface of the payment gateway
 */
trait PaymentGateway {
  def enterTransfer(request: TransferRequest): IO[TransferResponse]
  def checkTransferStatus(transferId: UUID): IO[Option[TransferStatus]]
  def getTransferResponse(transferId: UUID): IO[Option[TransferResponse]]
  def isPending(transferId: UUID): IO[Boolean] = for {
    response <- getTransferResponse(transferId)
    result = response.fold(false)(_.isPending)
  } yield result
}

sealed trait TransferStatus
case object Pending extends TransferStatus
case object Success extends TransferStatus
case object Failure extends TransferStatus

case class TransferRequest(amount: Double, senderAccount: Long, recipientAccount: Long)
case class TransferResponse(
  id: UUID,
  amount: Double,
  status: TransferStatus,
  senderAccount: Long,
  recipientAccount: Long) {
  val isPending = status == Pending
}

class MockPaymentGatewayService extends PaymentGateway {

  val logger: SelfAwareStructuredLogger[IO] = logging.getLogger

  private val transfers: Ref[IO, Map[UUID, TransferResponse]] = Ref.unsafe(Map.empty)

  /**
   * Infinite stream with a random delay, to mimic the behavior of a third party engine, which update the state of all
   * the pending transfers
   * @return
   */
  def startTransferUpdater(): IO[Unit] = {
    def updateTransfers(): IO[Unit] = for {
      _ <- logger.info("The third party service is updating the transfers")
      _ <- transfers.modify { currentTransfers =>
        val updatedTransfers = currentTransfers.view.mapValues { case elem =>
          elem.status match {
            case Pending => elem.copy(status = Success)
            case _ => elem
          }
        }.toMap
        (updatedTransfers, ())
      }
    } yield ()

    def randomIntervalStream: Stream[IO, Unit] =
      Stream
        .repeatEval(randomDelay())
        .flatMap(delay => Stream.sleep[IO](delay))
        .evalMap(_ => updateTransfers)

    randomIntervalStream.compile.drain
  }

  def enterTransfer(request: TransferRequest): IO[TransferResponse] =
    for {
      uuid <- UUIDGen[IO].randomUUID
      status = if (request.amount > 0) Pending else Failure
      response = TransferResponse(uuid, request.amount, status, request.senderAccount, request.recipientAccount)
      _ <- transfers.updateAndGet(_ + (uuid -> response))
    } yield response

  private def randomDelay(): IO[FiniteDuration] =
    Random.scalaUtilRandom[IO].flatMap(_.betweenInt(1000, 5000).map(Duration(_, MILLISECONDS)))

  def checkTransferStatus(transferId: UUID): IO[Option[TransferStatus]] =
    transfers.get.map(_.get(transferId).map(_.status))

  def getTransferResponse(transferId: UUID): IO[Option[TransferResponse]] =
    transfers.get.map(_.get(transferId))
}
