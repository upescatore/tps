CREATE TABLE IF NOT EXISTS accounts (
   account_number SERIAL PRIMARY KEY,
   balance DECIMAL NOT NULL
);

-- create_bank_transfer_table.sql
CREATE TABLE IF NOT EXISTS bank_transfers (
    transfer_uuid UUID PRIMARY KEY,
    transfer_state VARCHAR(20) NOT NULL,
    transferred_amount DECIMAL(15, 2) NOT NULL,
    sender_account_number SERIAL NOT NULL,
    recipient_bank_code INT NOT NULL,
    recipient_account_number INT NOT NULL
);

-- Generate 100 random balances and insert them into the accounts table with account numbers
-- Insert 100 rows into the accounts table
INSERT INTO accounts (balance)
SELECT (floor(RANDOM() * 10000))
FROM generate_series(1, 100);