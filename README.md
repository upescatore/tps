# TPS - Transfer Processing System

This guide will walk you through launching the TPS (Transfer Processing System) project. 
Before running the project, ensure that you have Docker installed and running on your machine.

## Setup

1. **Clone the Repository:** Clone the TPS project repository from the provided URL.

   ```bash
   git clone <repository-url>
   ```

2. **Navigate to Project Directory:** Change your current directory to the root directory of the cloned TPS project.

   ```bash
   cd <project-directory>
   ```

3. **Start PostgreSQL Server and Adminer:**

   Before launching the TPS project, you need to spin up a local PostgreSQL server and Adminer using Docker Compose. Execute the following command:

   ```bash
   docker-compose up -d
   ```

   This command will start a PostgreSQL container and an [Adminer](https://github.com/dockage/adminer) container in the background. 
   PostgreSQL will serve as the database backend for the TPS project, and Adminer will act as a lightweight client for PostgreSQL.
   To use Adminer, you can connect to http://localhost:9999 using the following parameters:

   ![Adminer Configuration](adminer_config.png)

## Running the Project

Once the PostgreSQL server and Adminer are up and running, you can proceed to run the TPS project.

To run the TPS project, execute the following command:

   ```bash
   sbt run
   ```

   This command will compile the TPS project and execute the main class specified in the project.


## Testing the Project

To test the functionality of the TPS project, you can use the provided Postman collection.

1. **Postman Collection:**

   In the project's `postman` directory, you will find a Postman collection file named `TPS.Postman_Collection.json`. Import this collection into your Postman application.

2. **Testing Endpoints:**

   Use the imported collection to test various endpoints of the TPS application. Ensure that the TPS project is running while testing the endpoints.

## Additional Commands

- **Compile the Project:**

  If you want to compile the TPS project without running it, you can execute the following command:

  ```bash
  sbt compile
  ```

- **Run Tests:**

  To run tests for the TPS project, execute:

  ```bash
  sbt test
  ```

---

# Development Assumptions

In the process of implementing the TPS (Transfer Processing System) project, several development assumptions were made. 
These assumptions were driven by:

- the aim to provide an overview of how to implement the system while focusing on essential aspects rather than diving into intricate details
- a couple of unclear requests from the requirements, that led me to some personal interpretations (e.g.: what the "transaction reference" is in the endpoint to accept request?)

Below are some of the main assumptions made during the development:

1. **Leveraging Typelevel Ecosystem for Concurrency/Parallelism:**

   The features of the Typelevel ecosystem were leveraged for handling concurrency and parallelism aspects of the project. 
   Utilizing libraries from the Typelevel ecosystem provides convenient and efficient solutions for these requirements with minimal programming overhead.

2. **Additional Endpoints for Retrieving Current Transfer State:**

   While the requirements did not explicitly mention endpoints for retrieving the current state of an entered transfer, 
   additional endpoints were added to GET this information. 
   This addition enhances the usability of the system by allowing users to query the current state of transfers.

3. **Naive Validation Provided by HTTP4S Encoder/Decoder:**

   Instead of implementing a more detailed validation mechanism using features like `Validated` applicative, 
   a simpler validation approach provided by HTTP4S encoder/decoder was utilized. 
   This choice was made due to the lack of specific requirements regarding validation expectations.

4. **Absence of OpenAPI Documentation:**

   OpenAPI documentation for the endpoints was not included in the project. 
   Although libraries like `smithy4s` or `guardrail` could have been used to generate OpenAPI documentation easily, 
   it was omitted to streamline the implementation process.

5. **Quick and Dirty Configuration via Docker-Compose for PostgreSQL:**

  Rather than setting up a more reliable configuration for PostgreSQL, 
  a quick and simple setup was implemented using Docker Compose. 
  This approach was chosen to expedite the setup process and focus more on the core functionality of the system.

6. **Application Configuration Held in Memory:**

   The application configuration is held in memory instead of using the classical HOCON/Pureconfig setup. 
   This decision was made based on the ambiguity in the requirements regarding the preferred configuration method: I chose to stay easy with this.

7. **Limited Test Coverage:**

   The test coverage is not very high, as the primary focus was to demonstrate familiarity with testing concepts 
   without putting extra effort solely for the sake of the exercise. 
   While tests were implemented to ensure basic functionality, extensive coverage was not pursued to keep the focus on the core implementation aspects.

These assumptions were made with the intention of providing a broad overview of the implementation process and my programming approach, 
while ensuring that the core functionality of the Transfer Processing System is adequately demonstrated. 

Needless to say that for a production-grade system, further refinement and adjustments may be necessary based on specific requirements and constraints.

---

**Note:** Ensure that you have the necessary prerequisites installed and configured before running the TPS project.